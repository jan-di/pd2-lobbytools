# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.3.0] - 2019-03-10
### Added
- Detailed log messages
- Ingame Hints are shown to all players now
- Compatibility with BLT 2
### Changed
- Improved Localization

## [1.2.0] - 2017-05-07
### Added
- German Translation
- Show ingame hint on Restart

## [1.1.0] - 2017-05-06
### Changed
- Prevent spamming an action multiple times

## [1.0.0] - 2017-04-19
### Added
- Initial Release