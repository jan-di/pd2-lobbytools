# Lobby Tools

A Payday 2 BLT-Mod which adds the following hotkeys:
- Instant Restart
- Force Ready (start game even if players are not ready)

All Hotkeys will work clientside, if the Host also have this mod and is your friend on Steam.

- [Changelog](CHANGELOG.md)
- [Sourcecode](https://gitlab.com/jan-di/pd2-lobbytools)
- [Report an Issue](https://gitlab.com/jan-di/pd2-lobbytools/-/issues)

Inspired by:
- [Useful Force Ready](http://www.nexusmods.com/payday2/mods/71/?) by kiraver.

## Installation

1. Install [SuperBLT Loader](https://superblt.znix.xyz/#installation)
2. Download the [Mod Archive](https://jan-di.gitlab.io/pd2-lobbytools/mod.zip)
3. Extract the files into your mods folder.

## Contributing

See [Contributing Instructions](CONTRIBUTING.md)