std = "luajit"

new_read_globals = {
    "Global",
    "ModPath",
    "SavePath",
    "managers",
    "ChatManager",
    "Network",
    "SystemInfo",
    "Hooks",
    "Utils",
    "Steam",
    "IngameWaitingForPlayersState",
    "game_state_machine",
    "DelayedCalls",
    "file",
    "Idstring",
    "log"
}

ignore = {
    "212/_.*" -- ignore arguments starting with an underscore. needed, to match the api functions
}