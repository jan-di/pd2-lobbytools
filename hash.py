# source: https://github.com/fragtrane/Python-SuperBLT-Hash-Calculator

import hashlib
import os

def hash(input_data, directory, BLOCKSIZE = 8192):
    hasher = hashlib.sha256()
    if directory:
        with open(input_data, 'rb') as file:
            buf = file.read(BLOCKSIZE)
            while len(buf) > 0:
                hasher.update(buf)
                buf = file.read(BLOCKSIZE)
    else:
        hasher.update(input_data)
    return hasher.hexdigest()

def dir(input_directory):
    hashes = dict()
    for r, _, f in os.walk(input_directory):
        for file in f:
            file_path = os.path.join(r, file)
            hashes[file_path.lower().encode('utf-8')] = hash(file_path, True)
    sorted_keys = sorted(hashes.keys())
    joined_hash = ""
    for key in sorted_keys:
        joined_hash = joined_hash + hashes[key]
    return hash(joined_hash.encode(), False)